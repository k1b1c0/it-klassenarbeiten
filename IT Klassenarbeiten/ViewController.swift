//
//  ViewController.swift
//  IT Klassenarbeiten
//
//  Created by Mirco Kibiger on 06.06.15.
//  Copyright (c) 2015 Blioxxx Development. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var webview: UIWebView!
    @IBOutlet weak var addExam: UIBarButtonItem!
    @IBOutlet weak var homeSite: UIBarButtonItem!
    @IBOutlet weak var homepage: UIBarButtonItem!
    
    //Functions
    func loadHomePage() {
        let requestURL = NSURL(string:homeURL)
        let request = NSURLRequest(URL: requestURL!)
        webview.loadRequest(request)
    }
    func loadStartURL() {
        let requestURL = NSURL(string:startURL)
        let request = NSURLRequest(URL: requestURL!)
        webview.loadRequest(request)
    }
    
    func loadAddURL() {
        let requestURL = NSURL(string:createURL)
        let request = NSURLRequest(URL: requestURL!)
        webview.loadRequest(request)
    }

    
    //Urls
    let homeURL = "http://schule.mircokibiger.de/index_app.php"
    let startURL = "http://schule.mircokibiger.de/klassenarbeiten"
    let createURL = "http://schule.mircokibiger.de/klassenarbeiten/eintragen.php"
    let creatorURL = "http://blixxx.net"
    
    //Applikation
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadHomePage()
        homepage.enabled = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //Button Actions
    @IBAction func homepage(sender: AnyObject) {
        loadHomePage()
        homeSite.enabled = true
        homepage.enabled = false
    }
    
    @IBAction func create(sender: AnyObject) {
        loadAddURL()
        addExam.enabled = false
        homeSite.enabled = true
    }
    
    @IBAction func start(sender: AnyObject) {
        loadStartURL ()
        homeSite.enabled = false
        addExam.enabled = true
        homepage.enabled = true
    }
}

class About:UIViewController {
    let creatorURL = "http://apps.blixxx.net/exams"

    func loadCreatorPage(){
        UIApplication.sharedApplication().openURL(NSURL(string: creatorURL)!)
        /*let requestURL = NSURL(string:creatorURL)
        let request = NSURLRequest(URL: requestURL!)
        webview.loadRequest(request)*/
    }

    @IBOutlet weak var CreatorWebsite: UIButton!
    
    @IBAction func cratorWebsite(sender: AnyObject) {
        loadCreatorPage();

    }
    
}

